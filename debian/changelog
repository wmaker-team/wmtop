wmtop (0.85-2) unstable; urgency=medium

  [ Doug Torrance ]
  * debian/control
    - Update Maintainer email address to tracker.d.o.
    - Bump Standards-Version to 4.6.0.
    - Update Homepage to dockapps.net.
    - Update Vcs-* after migration to Salsa.
    - Add Rules-Requires-Root field (no).
  * debian/copyright
    - Update Format URL to https.
    - Update upstream mailing list address.
    - Update Source to dockapps.net.
  * debian/menu
    - Remove deprecated Debian menu file.
  * debian/rules
    - Remove out-of-date get-orig-source target.
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.
  * debian/upstream/metadata
    - Add DEP-12 upstream metadata file.
  * debian/watch
    - Bump to version 4 format and use special strings for regular
      expressions.
    - Update download URL to dockapps.net.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dependency on dh-autoreconf.

 -- Doug Torrance <dtorrance@piedmont.edu>  Tue, 31 Aug 2021 20:39:40 -0400

wmtop (0.85-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Add dh-autoreconf, libdockapp-dev, and pkg-config to Build-Depends.
    - Bump Standards-Version to 3.9.7; no changes required.
    - Update Homepage.
    - Update Vcs-Browser to use https protocol.
  * debian/copyright
    - Update Upstream-Contact and Source.
    - Remove license information for wmgeneral directory; no longer exists.
  * debian/patches
    - Remove directory; patches applied upstream or made unnecessary due to
      switch to autotools.
  * debian/rules
    - Enable all hardening flags.
    - Use dh_autoreconf and remove override_dh_auto_{build,install} targets
      made unnecessary because of switch to autotools.
    - Add get-orig-source target.
  * debian/watch
    - Update with new download location.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 08 Feb 2016 20:23:11 -0500

wmtop (0.84-12) unstable; urgency=medium

  * debian/control
    - Update Vcs-* fields.
    - Set Debian Window Maker Team as Maintainer; move myself to
      Uploaders with updated email address.
  * debian/{copyright,patches}
    - Update email address.

 -- Doug Torrance <dtorrance@piedmont.edu>  Tue, 25 Aug 2015 20:57:48 -0400

wmtop (0.84-11) unstable; urgency=medium

  * debian/patches/remove_inline_keywords.patch
    - New patch; allow builds using gcc5 (Closes: #793136).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Tue, 21 Jul 2015 21:12:51 -0600

wmtop (0.84-10) unstable; urgency=medium

  * New maintainer (Closes: #792324).
  * debian/compat
    - Bump to 9.
  * debian/control
    - Bump versioned dependency on debhelper to >= 9.
    - Remove dpatch from Build-Depends.
    - Bump Standards-Version to 3.9.6.
    - Add Vcs-*.
  * debian/copyright
    - Convert to DEP5 format.
  * debian/docs
    - New file.
  * debian/patches/01_restore_pristine_code.dpatch
    - Split into to new quilt patches, comment_strip_of_binary.patch and
      fix_incorrect_memory_usage.patch.
  * debian/patches/10_fix_manpage.dpatch
    - Rename as fix_manpage.patch, convert to quilt, and add DEP3 header.
    - Fix spelling error.
  * debian/patches/fix_install.patch
    - New patch; honor DESTDIR environment variable, create installation
      directories, and install manpage according to FHS.
  * debian/patches/honor_buildflags.patch
    - Honor CPPFLAGS, CFLAGS, and LDFLAGS.
  * debian/README.source
    - Remove file; unnecessary as package no longer uses dpatch.
  * debian/rules
    - Update to use dh.
  * debian/source/format
    - New file; use 3.0 (quilt).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Mon, 13 Jul 2015 16:48:58 -0600

wmtop (0.84-9) unstable; urgency=low

  * QA upload.
  * debian/control
    - Set maintainer to QA Group.
    - Bump Standards-Version to 3.8.0.
      + Add debian/README.source.
    - Uncapitalize short description.
    - Remove additional spaces from long description (paragraph fill).
    - Add ${misc:Depends} to binary package Depends.
  * debian/rules
    - Remove 'configure' from PHONY, since the target does not exist anymore.
    - Merge 'rm' call into 'dh_clean' one.
  * debian/patches/10_fix_manpage.dpatch
    - Fix another minus sign warning.

 -- Sandro Tosi <morph@debian.org>  Sun, 28 Dec 2008 17:07:00 +0100

wmtop (0.84-8) unstable; urgency=low

  * debian/control
    - Bump Standards-Version to 3.7.3.
    - Move Homepage field from binary to source stanza.
    - Add build-depend against libxext-dev since libx11-dev will no longer
      depends on it.
    - Add dpatch dependency.
  * debian/rules
    - Add dpatch stuff.
    - Little change to clean target.
    - Add direct docs installation.
  * debian/patches/01_restore_pristine_code.dpatch
    - Add to restore pristine source code.
  * debian/patches/10_fix_manpage.dpatch
    - Add to escape minus signes in manpage (lintian warning).
  * debian/dirs
    - Remove.
  * debian/docs
    - Remove.
  * debian/copyright
    - Clear separation of license and copyright.
    - Upstream author, license and copyright indented with 4 spaces.
    - Link to local GPLv2 license text fixed.

 -- Sandro Tosi <matrixhasu@gmail.com>  Wed, 09 Jan 2008 20:04:01 +0100

wmtop (0.84-7) unstable; urgency=low

  * Comment strip of binary from upstream Makefile and debian/rules
    (Closes: #438278).
  * Add Tony Mancill to Uploaders in debian/control.
  * Remove cruft from bottom of debian/changelog.
  * Update menu section to Applications/System/Monitoring.
  * Update debian/rules to no longer ignore errors from $(MAKE) clean.

 -- tony mancill <tmancill@debian.org>  Thu, 16 Aug 2007 22:46:37 -0700

wmtop (0.84-6) unstable; urgency=low

  * debian/watch
    - Url correction.

 -- Sandro Tosi <matrixhasu@gmail.com>  Sat, 16 Sep 2006 10:45:42 +0200

wmtop (0.84-5) unstable; urgency=low

  * debian/watch
    - Add.
  * debian/compat
    - Add, removing DH_COMPAT from debian/rules.
  * debian/rules
    - Remove DH_COMPAT and commented dh_* commands.
    - Add explicit manpage in dh_installman.
  * wmtop.manpages
    - Remove and add the manpage in debian/rules.
  * debian/control
    - New maintainer (Closes: #347529).
    - Push Standards-Version to 3.7.2.
    - Modify long description taking it from webpage.
    - Add Homepage field.
  * debian/copyright
    - Change site url where to donwload upstream package, and its email
      address.
    - Add copyright notes for wmgeneral/*.
    - Add me as new maintainer.

 -- Sandro Tosi <matrixhasu@gmail.com>  Sun, 27 Aug 2006 19:07:38 +0200

wmtop (0.84-4) unstable; urgency=low

  * Orphan package.
  * Modify build-depends (s/xlibs-dev/libx11-dev,libxpm-dev/).

 -- Hugo van der Merwe <hvdm@debian.org>  Wed, 11 Jan 2006 12:25:47 +0200

wmtop (0.84-3) unstable; urgency=low

  * Fix wmtop memory reported for kernel 2.6 (Closes: #224732)
    (Thanks Dwayne C. Litzenberger).

 -- Hugo van der Merwe <hvdm@debian.org>  Sat,  3 Jan 2004 23:43:38 +0200

wmtop (0.84-2) unstable; urgency=low

  * Remove /usr/man/man1 dir from package.

 -- Hugo van der Merwe <hvdm@debian.org>  Sun, 28 Oct 2001 17:11:24 +0200

wmtop (0.84-1) unstable; urgency=low

  * New upstream release.

 -- Hugo van der Merwe <hvdm@debian.org>  Wed, 24 Oct 2001 22:04:38 +0200

wmtop (0.7-2) unstable; urgency=low

  * New maintainer (Closes: #98251).
  * Install upstream man page (Closes: #93474).
  * Remove Suggests: wmaker (Closes: #82998).
  * Update a lot of debian/ files, as inspired by dh_make, update to
    Standards-Version: 3.5.2
  * Not uploaded, found new upstream release

 -- Hugo van der Merwe <hvdm@debian.org>  Wed, 24 Oct 2001 19:17:24 +0200

wmtop (0.7-1) unstable; urgency=low

  * Initial release.

 -- Edward C. Lang <edlang@debian.org>  Fri, 14 Apr 2000 19:25:35 +1000
